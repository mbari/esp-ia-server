FROM openjdk:8-jdk

MAINTAINER Brian Schlining <bschlining@gmail.com>

ENV APP_HOME /opt/esp-ia-server

RUN mkdir -p ${APP_HOME}

COPY target/pack/ ${APP_HOME}/

EXPOSE 8080

ENTRYPOINT $APP_HOME/bin/jetty-main