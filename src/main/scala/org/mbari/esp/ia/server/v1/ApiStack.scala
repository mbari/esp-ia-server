package org.mbari.esp.ia.server.v1

import org.json4s.{DefaultFormats, Formats}
import org.scalatra.{ContentEncodingSupport, FutureSupport, ScalatraServlet}
import org.scalatra.json.JacksonJsonSupport
import org.scalatra.servlet.FileUploadSupport
import org.slf4j.{Logger, LoggerFactory}

/**
 * @author Brian Schlining
 * @since 2017-11-16T14:21:00
 */
abstract class ApiStack
    extends ScalatraServlet
    with JacksonJsonSupport
    with ContentEncodingSupport
    with FutureSupport
    with FileUploadSupport {

  protected[this] val log: Logger = LoggerFactory.getLogger(getClass)
  protected[this] implicit lazy val jsonFormats: Formats = DefaultFormats
}
