package org.mbari.esp.ia.server.v1

import java.io.{BufferedOutputStream, File, FileOutputStream}

import org.mbari.esp.ia.analysis.AutoAnalyzeFn
import org.mbari.esp.ia.services.{GeneArrayIOService, GeneArrayIOServiceImpl, ImageIOServiceB}
import org.mbari.esp.ia.util.Zip
import org.mbari.io.IOUtilities
import org.scalatra.BadRequest
import org.scalatra.servlet.MultipartConfig

import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source

/**
  * @author Brian Schlining
  * @since 2017-11-16T14:15:00
  */
class AnalysisV1Api(maxFileSizeGB: Int = 3,
                    defaultBlockSize: Int = 5,
                    defaultSpotRadius: Int = 8)(implicit val executor: ExecutionContext) extends ApiStack {

  // Configure Max upload size
  configureMultipartHandling(MultipartConfig(maxFileSize = Some(maxFileSizeGB*1024*1024)))

  private[this] implicit val geneArrayIO: GeneArrayIOService = new GeneArrayIOServiceImpl
  private[this] val imageIO = new ImageIOServiceB

  get("/analyze") {
    contentType = "text/html"

    <html>
      <head><title>Analyze image</title></head>
      <body>
        <h1>Analyze image</h1>
        <form action="analyze" method="post" enctype="multipart/form-data">
          <p>
            <label for="image">TIF Image</label>
            <input type="file" name="image" id="image" />
          </p>
          <p>
            <label for="gal">GAL File</label>
            <input type="file" name="gal" id="gal" />
          </p>
          <p>
            <label for ="fiducialKey">Fiducial Key in GAL</label>
            <input type="text" name="fiducialKey" id="fiducialKey" />
          </p>
          <input type="submit" value="Analyze" name="submit" />
        </form>
      </body>
    </html>
  }

  post("/analyze") {
    Future {

      log.debug("--- Start analysis")
      val fiducialKey = params.get("fiducialKey")
        .getOrElse(halt(BadRequest(body = "A fiducialKey parameter is required")))
      val blockSize = params.getAs[Int]("blockSize").getOrElse(defaultBlockSize)
      val spotRadius = params.getAs[Int]("spotRadius").getOrElse(defaultSpotRadius)

      log.debug("--- Reading image")
      val bufferedImage = fileParams.get("image") match {
        case Some(file) =>
          // Copy the file locally so that JAI can read it
          val tempFile = File.createTempFile("temporary-image", ".tif")
          log.debug(s"Writing TIF to {}", tempFile)
          val inputStream = file.getInputStream
          val outputStream = new BufferedOutputStream(new FileOutputStream(tempFile))
          IOUtilities.copy(inputStream, outputStream)
          inputStream.close()
          outputStream.close()
          val img = imageIO.readAsBufferedImage(tempFile.toURI.toURL)
          tempFile.delete()
          img
        case None =>
          halt(BadRequest(body = "no image was uploaded"))
      }

      log.debug("--- Reading gal")
      val galLines = fileParams.get("gal") match {
        case Some(file) =>
          val inputStream = file.getInputStream
          val gal = Source.fromInputStream(inputStream)
            .getLines()
            .toList
          inputStream.close()
          gal
        case None =>
          halt(BadRequest(body = "no gal was uploaded"))
      }

      contentType = "application/octet-stream"
      response.setHeader("Content-Disposition","attachment; filename=Results.zip")

      log.debug("--- Analyzing image")
      val (resultImage, resultText) = AutoAnalyzeFn(bufferedImage,
        galLines,
        fiducialKey,
        spotRadius,
        blockSize)

      log.debug("--- Zipping results")
      Zip(resultImage, resultText)

    }
  }

}
