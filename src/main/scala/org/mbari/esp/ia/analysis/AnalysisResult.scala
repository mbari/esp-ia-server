package org.mbari.esp.ia.analysis

import ij.process.ImageProcessor
import org.mbari.esp.ia.geometry.{GALPoint, Point3D}
/**
  * @author Brian Schlining
  * @since 2017-11-16T15:34:00
  */
case class AnalysisResult(imageProcessor: ImageProcessor,
                          intensities: Map[GALPoint, Point3D[Double]],
                          spotRadius: Float,
                          blockSize: Int)
