package org.mbari.esp.ia.analysis

import java.awt.image.BufferedImage

import ij.ImagePlus
import org.mbari.esp.ia.ga.impl3.ESPAutoAligner
import org.mbari.esp.ia.geometry.{GALPoint, LabeledDoublePoint2D}
import org.mbari.esp.ia.imglib.{BrightSpotExtractorFn, MultiPassRegionsExtractorFn}
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, GeneArrayIOService}
import org.slf4j.LoggerFactory

/**
  * @author Brian Schlining
  * @since 2017-11-16T15:30:00
  */
class ImageAnalyzer(implicit geneArrayIOService: GeneArrayIOService) {

  private[this] val log = LoggerFactory.getLogger(getClass)

  def apply(image: BufferedImage,
            galLines: Seq[String],
            fiducialKey: String,
            spotRadius: Int,
            blockSize: Int): AnalysisResult = {

    val imagePlus = new ImagePlus("esp", image)
    val allGalPoints = geneArrayIOService.read(galLines)
    // --- Isolate a single filter from the GAL points
    val galPoints = GeneArrayAnalysisService.filterSingleSpot(allGalPoints, 12500)

    val fiducialPoints = extractFiducialPoints(galPoints, fiducialKey)
    val fiducialImagePoints = extractFiducialSpots(imagePlus)
    val allImagePoints = extractImageSpots(imagePlus)
    val aligner = new ESPAutoAligner(fiducialPoints, galPoints)
    val alignedLabeledGalPoints = aligner(imagePlus.getWidth, imagePlus.getHeight, fiducialImagePoints,
      allImagePoints)

    // Convert the LabeledIntPoint2D back to the correct, but aligned, GALPoint
    val alignedGalPoints = alignedLabeledGalPoints.map { lp =>
      val gp = galPoints.filter(_.spotNumber == lp.label).head
      new GALPoint(gp, lp.x, lp.y)
    }

    // Extract the intensity information from the image
    val results = GeneArrayAnalysisService.extractIntensities(imagePlus.getProcessor, alignedGalPoints,
      spotRadius, blockSize)

    AnalysisResult(imagePlus.getProcessor, results, spotRadius, blockSize)

  }



  private def extractFiducialSpots(imagePlus: ImagePlus): Seq[LabeledDoublePoint2D] = {
    BrightSpotExtractorFn(imagePlus.getProcessor).map(_.centroid()).toSeq
  }


  private def extractImageSpots(imagePlus: ImagePlus): Seq[LabeledDoublePoint2D] = {
    MultiPassRegionsExtractorFn(imagePlus.getProcessor).map(_.centroid()).toSeq
  }

  private def extractFiducialPoints(galPoints: Seq[GALPoint], fiducialKey: String): Seq[GALPoint] = {
    val tmpPts = galPoints.filter(_.sourceWell == fiducialKey)
    if (tmpPts.size == 3) {
      tmpPts
    }
    else if (tmpPts.size > 3) {
      tmpPts.take(3)
    }
    else if (tmpPts.size == 2) {
      log.warn("Only 2 fiducial points were found in the GALFile with the id of " +
        fiducialKey + ". We expected at least 3. We'll try to work with it though.")
      tmpPts :+ tmpPts.head // Reuse the 1st point so we have 3 points in our seq
    }
    else {
      throw new IllegalArgumentException("The GAL file at needs to have at" +
        "least 2 points labeled '" + fiducialKey + "' but we found " + tmpPts.size)
    }
  }
}
