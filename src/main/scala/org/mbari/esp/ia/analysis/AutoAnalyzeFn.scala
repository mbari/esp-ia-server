package org.mbari.esp.ia.analysis

import java.awt.image.BufferedImage
import java.text.SimpleDateFormat

import org.mbari.esp.ia.services.{GeneArrayAnalysisService, GeneArrayIOService}
import org.slf4j.LoggerFactory

/**
  * @author Brian Schlining
  * @since 2017-11-17T12:27:00
  */
object AutoAnalyzeFn {

  private[this] val df = new SimpleDateFormat("yyyyMMddHHmmss")
  private[this] val log = LoggerFactory.getLogger(getClass)

  def apply(image: BufferedImage,
            galLines: Seq[String],
            fiducialKey: String,
            spotRadius: Int,
            blockSize: Int)(implicit geneArrayIOService: GeneArrayIOService):
  (BufferedImage, String) = {

    val analyzer = new ImageAnalyzer
    log.debug("Starting analysis")
    val results = analyzer(image, galLines, fiducialKey, spotRadius, blockSize)
    log.debug("Creating results image")
    val resultImage = GeneArrayAnalysisService.createIntensityImage(results.imageProcessor,
      results.intensities, results.blockSize)
    log.debug("Creating results text")
    val resultText = GeneArrayAnalysisService.createIntensityData(results.imageProcessor,
      results.intensities, results.spotRadius, results.blockSize)

    (resultImage, resultText)
  }

}
