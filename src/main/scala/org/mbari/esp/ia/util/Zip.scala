package org.mbari.esp.ia.util

import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets
import java.util.zip.{ZipEntry, ZipOutputStream}
import javax.imageio.ImageIO

import org.mbari.io.IOUtilities

/**
  * @author Brian Schlining
  * @since 2017-11-17T14:07:00
  */
object Zip {

  def apply(image: BufferedImage, text: String): Array[Byte] = {
    val bytes = new ByteArrayOutputStream()
    val zip = new ZipOutputStream(bytes)

    // Write image to zip
    zip.putNextEntry(new ZipEntry("Results.png"))
    ImageIO.write(image, "png", zip)
    zip.closeEntry()

    // Write text to zip
    zip.putNextEntry(new ZipEntry("Results.txt"))
    zip.write(text.getBytes(StandardCharsets.UTF_8))
    zip.closeEntry()

    zip.close()
    bytes.toByteArray // return zip as byte array
  }

}
