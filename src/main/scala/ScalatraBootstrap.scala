import javax.servlet.ServletContext

import com.typesafe.config.{Config, ConfigFactory}
import org.mbari.esp.ia.server.v1.AnalysisV1Api
import org.scalatra.LifeCycle
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext

/**
 *
 *
 * @author Brian Schlining
 * @since 2016-05-20T14:41:00
 */
class ScalatraBootstrap extends LifeCycle {

  private[this] val log = LoggerFactory.getLogger(getClass)

  override def init(context: ServletContext): Unit = {

    log.info("STARTING UP NOW")

    implicit val ec: ExecutionContext = ExecutionContext.global

    val config = ConfigFactory.load()
    val maxFileSize = config.getInt("analysis.filesize.max.gb")
    val blockSize = config.getInt("analysis.block.size")
    val spotRadius = config.getInt("analysis.spot.radius")
    val analysisV1Api = new AnalysisV1Api(maxFileSize, blockSize, spotRadius)
    context.mount(analysisV1Api, "/")
//    val imageV1Api = new ImageV1Api(config.getInt("panoptes.max.size.gb"))
//    val authApi = new AuthorizationV1Api
//
//    context.mount(imageV1Api, "/v1/images")
//    context.mount(authApi, "/v1/auth")

  }

}
