package org.mbari.esp.ia.analysis

import java.io.{File, PrintWriter}
import javax.imageio.ImageIO

import org.mbari.esp.ia.services.{GeneArrayIOService, GeneArrayIOServiceImpl, ImageIOServiceB}
import org.scalatest.{FlatSpec, Matchers}

import scala.io.Source

/**
  * @author Brian Schlining
  * @since 2017-11-17T12:34:00
  */
class AnalysisWalkthrough extends FlatSpec with Matchers {

  "AnalysisWalkthrough" should "run auto-alignment "in {

    val galUrl = getClass.getResource("/2017_Seattle_ESP_Eddie_images/HAB_Arrray_NOAA_17mar09.gal")
    val galLines = Source.fromURL(galUrl).getLines().toList
    val imageUrl = getClass.getResource("/2017_Seattle_ESP_Eddie_images/hab17oct1016h1000ml40s.tif")
    val io = new ImageIOServiceB
    val image = io.readAsBufferedImage(imageUrl)

    println(imageUrl)

    implicit val geneArrayIO: GeneArrayIOService = new GeneArrayIOServiceImpl
    val (resultImage, resultText) = AutoAnalyzeFn(image, galLines, "Alex_comp", 8, 4)

    val imageIO = new ImageIOServiceB
    imageIO.write(resultImage, new File("target", getClass.getSimpleName + "-results.png"))

    val writer = new PrintWriter(new File("target", getClass.getSimpleName + "-results.txt"))
    writer.write(resultText)
    writer.close()
  }

}

