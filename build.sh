#!/usr/bin/env bash

sbt pack &&
    docker build -t mbari/esp-ia-server . &&
    docker push mbari/esp-ia-server