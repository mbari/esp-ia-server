# esp-ia-server
## Build
 
### Using sbt

build: `sbt pack`   
run: `sbt jetty:start`  

### Using docker

build: `build.sh`  
run: `docker --name esp -p 8080:8080 mbari/esp-ia-server`
 
## Try out
 
 Browse to <http://localhost:8080/analyze>

## Footnotes

I initially looked at using [akka-http](https://doc.akka.io/docs/akka-http/current/scala/http/) but it [doesn't support uploading mulitple files in the current release.](https://github.com/akka/akka-http/issues/1273)